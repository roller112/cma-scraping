let rp = require('request-promise');
let cheerio = require('cheerio');
let moment = require('moment');
const readline = require('readline');

let searchKeyword = 'CAD0356601';

const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const webUrl = 'https://www.cma-cgm.com/ebusiness/tracking/search?SearchBy=BL&Reference=';
const googleAPI = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json';
const googleDetailAPI = 'https://maps.googleapis.com/maps/api/place/details/json';
const googleKEY = 'AIzaSyDJXD_kg9-QouZDczKsxDogDh9zjVvKdHU';


let getAddressFromPort = async (portName) => {
  const options2 = {
    method: 'GET',
    uri: googleAPI,
    qs: {
      input: portName,
      inputtype: 'textquery',
      key: googleKEY
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true
  };
  
  const placeIDRes = await rp(options2);

  if (placeIDRes.candidates.length === 0) {
    const options1 = {
      method: 'GET',
      uri: 'https://geocoder.api.here.com/6.2/geocode.json',
      // jar: cookiejar,
      qs: {
        app_id: '3Wt8KnudGvHpJyttt9Ue',
        app_code: 'SVqP5rpzTzy7sn6m4Fe5AQ',
        searchtext: portName
      },
      headers: {
          'User-Agent': 'Request-Promise'
      },
      json: true
    };

    const temp = await rp(options1);
    if (temp.Response && temp.Response.View && temp.Response.View.length > 0 && temp.Response.View[0].Result
      && temp.Response.View[0].Result.length > 0 && temp.Response.View[0].Result[0].Location.Address) {
      return {
        country: temp.Response.View[0].Result[0].Location.Address.Country || '',
        region: temp.Response.View[0].Result[0].Location.Address.County || '',
        city: temp.Response.View[0].Result[0].Location.Address.City || '',
      }
    }
    return {};
    
  } else {
    const options3 = {
      method: 'GET',
      uri: googleDetailAPI,
      qs: {
        placeid: placeIDRes.candidates[0].place_id,
        fields: 'address_components,formatted_address,name',
        key: googleKEY
      },
      headers: {
        'User-Agent': 'Request-Promise',
      },
      json: true
    };
    const placeDetail = await rp(options3);
    return getFormattedAddress(placeDetail.result.address_components);
  }
};

// getAddressFromPort('KINGSTON');

let getFormattedAddress = (addressArr) => {
  let country = '';
  let region = '';
  let city = '';
  for (let i = 0; i < addressArr.length; i++) {
    if (addressArr[i].types.includes('country')) {
      country = addressArr[i].short_name;
    }
    if (addressArr[i].types.includes('administrative_area_level_1')) {
      region = addressArr[i].short_name;
    }
    if (addressArr[i].types.includes('locality')) {
      city = addressArr[i].short_name;
    }
  }
  return {
    country,
    region,
    city
  };
}

r1.question('Input tracking number, container number or BOL number:', (input) => {
  console.log('[---Searching for ---]', input);
  searchKeyword = input;

  if (input) {
    console.log('[---Start scrapping----]');
    scrape().then(value => {
      console.log('[---value---]', value);
    });
  }

  r1.close();
})

let scrape = async () => {

  const options = {
    method: 'POST',
    uri: `${webUrl}${searchKeyword}&search=Search`,
    formData: {
      dt_knd: 'BL',
      own_yn: 'N',
      flag: 'O',
      condition: 'BL',
      bl_no: searchKeyword
    },
    transform: function (body) {
      return cheerio.load(body);
    }
  };

  let $ = await rp(options);

  $.prototype.textln = function () {
    this.find('br').replaceWith('\n');
    this.find('*').each(function () {
      $(this).replaceWith($(this).html());
    });
    return this.html();
  }

  let rows = $('#t1').find('tr');
  let output = {};
  let flow = [];

  if (rows.length < 2) {
    return {};
  }

  let current = rows[1];
  let cells = $(current).find('td');
  
  const containerNameCell = $(cells[1]).find('a');
  console.log('[---containerName---]', $(containerNameCell).text());
  const containerName = $(containerNameCell).text();

  const options1 = {
    method: 'POST',
    uri: `https://www.cma-cgm.com/eBusiness/tracking/detail/${containerName}?SearchCriteria=BL&SearchByReference=${searchKeyword}`,
    formData: {
      dt_knd: 'BL',
      own_yn: 'N',
      flag: 'O',
      condition: 'BL',
      bl_no: searchKeyword
    },
    transform: function (body) {
      return cheerio.load(body);
    }
  };

  $ = await rp(options1);

  output.bl = searchKeyword;

  rows = $('#container-moves').find('tbody tr');
  for (let i = 0; i < rows.length; i++) {
    current = rows[i];

    cells = $(current).find('td');
    
    let moveDate = $(cells[0]).text();
    let moveStatus = $(cells[1]).text();
    let moveLocation = await getAddressFromPort($(cells[2]).text());
    let movePort = $(cells[2]).text().replace(/\r\n|\r/g, '').replace(/\n/g, '').replace(/\t/g, '');
    let moveVeseel = $(cells[3]).text();
    moveVeseel = moveVeseel.replace(/\r\n|\r/g, '').replace(/\n/g, '').replace(/\t/g, '');
    let moveVoyage = $(cells[4]).text();


    flow.push({
      date: moveDate,
      location: {
        ...moveLocation,
        port: movePort
      },
      vessel: moveVeseel,
      voyage: moveVoyage,
      status: moveStatus
    });
  }

  // console.log('[---flow---]', flow);

  let destination = {};
  let currentLocation = {};
  let originalLocation = flow[0].location;
  let status = 'IN-DELIVERY';
  let eta = '';
  let currentVessel = '';
  let currentVoyage = '';
  const cDate = new Date();

  for (let i = 0; i < flow.length; i ++ ){
    const fDate = moment(flow[i].date, 'ddd DD MMM YYYY HH:mm');

    if (i === flow.length - 1) {
      eta = moment(flow[i].date, 'ddd DD MMM YYYY HH:mm');
      destination = flow[i].location;
      if (cDate >= fDate) {
        status = 'COMPLETE';
      }
    }

    if (cDate >= fDate) {
      currentLocation = flow[i].location;
      currentVessel = flow[i].vessel;
      currentVoyage = flow[i].voyage;
    }
  }

  return {
    bl: output.bl,
    vesselName: currentVessel,
    voygaeNumber: currentVoyage,
    eta: moment(eta).format(moment.HTML5_FMT.DATETIME_LOCAL_MS),
    status,
    locations: [
      originalLocation,
      {
        ...currentLocation,
        isCurrent: true
      },
      destination
    ]
  };
};